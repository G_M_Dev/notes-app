const fs = require('fs')
const chalk = require('chalk')

const addNote = (title, body) => {
    const notes = loadNotes()
    const duplicateNote = notes.find((note) => {
        const isDuplicate = note.title === title
        if (isDuplicate) { note.body = body }
        return isDuplicate
    })
    if (!duplicateNote) {
        notes.push({
            title: title,
            body: body
        })
        saveNotes(notes)
        console.log(chalk.green.inverse('New note added!'))
    }
    else {
        saveNotes(notes)
        console.log(chalk.yellow.inverse('Note body overwritten!'))
    }
}

const removeNote = (title) => {
    const notes = loadNotes()
    const notesToKeep = notes.filter((note) => note.title !== title)    
    if (notesToKeep.length !== notes.length) { 
        saveNotes(notesToKeep)
        console.log(chalk.green.inverse('Note removed!'))
    }
    else {
        console.log(chalk.red.inverse('Note not found!'))
    }
}

const listNotes = () => {
    const notes = loadNotes()
    console.log(chalk.inverse('Your notes'))
    notes.forEach((note) => {
        console.log("- " + note.title)
    })
}

const readNote = (title) => {
    const notes = loadNotes()
    const pickedNote = notes.find((note) => note.title===title)
    if(pickedNote) {
        console.log(chalk.inverse(pickedNote.title))
        console.log(pickedNote.body)
    }
    else {
        console.log(chalk.red.inverse('Note not found!'))
    }
}

const saveNotes = (notes) => {
    fs.writeFileSync('notes.json', JSON.stringify(notes))
}

const loadNotes = () => {
    try {
        const dataBuff = fs.readFileSync('notes.json')
        const dataJSON = dataBuff.toString()
        return JSON.parse(dataJSON)
    } catch(e) {
        return []
    }
}

module.exports = {
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes,
    readNote: readNote
}